from datetime import datetime
import cx_Oracle
import ldap
import ldap.filter
import re
from flask import Flask, render_template, request, redirect, session, flash, url_for
from dao import CensoDao
from models import Censo
import subprocess

app = Flask(__name__)

app.secret_key = 'LlX$3h5GH69Xdaaf'

pwd = "dbapssta368"
usr = "dbaps"
dsn = "10.200.0.211/PRDME"

lista_hospitais = sorted(['ANTONIO AFONSO', 'ALVORADA', 'FREI GALVÃO', 'REGIONAL', 'SANTA CASA DE PINDA',
                          'SANTA CASA DE GUARATINGUETA', 'SANTA CASA DE LORENA', 'SANTA CASA DE CRUZEIRO',
                          'SANTA CASA DE UBATURA', 'SANTA CASA DE SÃO SEBASTIÃO', 'SANTA CASA DE SÃO JOSÉ DOS CAMPOS'
                          'STELLA MARIS', 'GAAC', 'VALE PARAIBANO', 'GUARAREMA', 'BENEFICENCIA PORTUGUESA',
                          'SALVALUS'
                          ])

censo_dao = CensoDao(usr=usr, pwd=pwd, dsn=dsn)


@app.route('/')
def index():
    if 'usuario_logado' not in session or session['usuario_logado'] is None:
        return redirect(url_for('autenticarPorLdap', proxima=url_for('index')))

    censos = censo_dao.listar()

    return render_template('lista.html', censos=censos, lista_hospitais=lista_hospitais, titulo='Censo Hospitalar')


@app.route('/criar', methods=['POST', ])
def criar():
    nr_guia = request.form['nr_guia']
    acomodacao = request.form['acomodacao']
    ds_hospital = request.form['ds_hospital']
    beneficiario = request.form['beneficiario']
    plano = request.form['plano']
    classificacao = request.form['classificacao']
    quadro_clinico = request.form['quadro_clinico']
    dt_entrada = re.sub(r'(\d{4})-(\d{1,2})-(\d{1,2})', '\\3-\\2-\\1', request.form['dt_entrada'])
    dt_saida = re.sub(r'(\d{4})-(\d{1,2})-(\d{1,2})', '\\3-\\2-\\1', request.form['dt_saida'])
    diagnostico = request.form['diagnostico']
    ultima_modificacao = session['usuario_logado'] + ' - ' + str(datetime.now())

    censo = Censo(
        nr_guia,
        acomodacao,
        ds_hospital,
        beneficiario,
        plano,
        classificacao,
        quadro_clinico,
        dt_entrada,
        dt_saida,
        diagnostico,
        ultima_modificacao
    )
    censo_dao.salvar(censo)
    print(session['usuario_logado'] + ' - /criar')
    return redirect(url_for('index'))


@app.route('/editar/<int:id>')
def editar(id):
    print(session['usuario_logado'] + ' - /editar')
    if 'usuario_logado' not in session or session['usuario_logado'] == None:
        return redirect(url_for('login', proxima=url_for('editar')))

    censo = censo_dao.busca_por_id(id)

    if censo.dt_entrada is not None or censo.dt_entrada == '':
        censo.dt_entrada = censo.dt_entrada[6:10] + '-' + censo.dt_entrada[3:5] + '-' + censo.dt_entrada[0:2]
    if censo.dt_saida is not None or censo.dt_saida == '':
        censo.dt_saida = censo.dt_saida[6:10] + '-' + censo.dt_saida[3:5] + '-' + censo.dt_saida[0:2]

    return render_template('editar.html', titulo='Editando Registro',lista_hospitais = lista_hospitais , censo=censo)


@app.route('/buscar/<int:id>', methods=['GET'])
def buscarPorId(id):
    print(id)
    censo = censo_dao.busca_por_id(id).__dict__
    # censo_schema = CensoSchema()
    # censo_dict = censo_schema.dump(censo)

    return censo


@app.route('/atualizar', methods=['POST', ])
def atualizar():
    nr_guia = request.form['nr_guia']
    acomodacao = request.form['acomodacao']
    ds_hospital = request.form['ds_hospital'].upper()
    beneficiario = request.form['beneficiario'].upper()
    plano = request.form['plano']
    classificacao = request.form['classificacao']
    quadro_clinico = request.form['quadro_clinico']
    dt_entrada = re.sub(r'(\d{4})-(\d{1,2})-(\d{1,2})', '\\3-\\2-\\1', request.form['dt_entrada'])
    dt_saida = re.sub(r'(\d{4})-(\d{1,2})-(\d{1,2})', '\\3-\\2-\\1', request.form['dt_saida'])
    diagnostico = request.form['diagnostico']
    ultima_modificacao = session['usuario_logado'] + ' - ' + str(datetime.now())
    id = request.form['id']

    censo = Censo(
        nr_guia,
        acomodacao,
        ds_hospital,
        beneficiario,
        plano,
        classificacao,
        quadro_clinico,
        dt_entrada,
        dt_saida,
        diagnostico,
        ultima_modificacao,
        id=id
    )

    censo_dao.salvar(censo)
    print(session['usuario_logado'] + ' - /atualizar')
    return redirect(url_for('index'))


@app.route('/deletar/<int:id>')
def deletar(id):
    censo_dao.deletar(id)
    flash('Registro removido com sucesso!')
    print(session['usuario_logado'] + ' - /deletar/<int:id>')
    return redirect(url_for('index'))


@app.route('/login')
def login():
    proxima = request.args.get('proxima')
    return render_template('login.html', proxima=proxima)


@app.route('/autenticacaomanual', methods=['post', ])
def autenticacaoManual():
    list_usuario = ['mlarosario', 'cobarrinha', 'lgggrangeiro']
    if request.form['usuario'] in list_usuario:
        con = ldap.initialize("ldap://10.201.0.250:389")
        user_dn = request.form['usuario'] + "@ascs.intra"
        password = request.form['senha']

        try:
            con.simple_bind_s(user_dn, password)
            flash(user_dn + ' Logou com sucesso! ', 'alert-success')
            proxima_pagina = request.form['proxima']
            print(user_dn + ' - /autenticacaomanual')
            session['usuario_logado'] = user_dn
            return redirect(proxima_pagina)
        except ldap.LDAPError as e:
            print(e)
            flash('Usuario incorreto, tente novamente!', 'alert-danger')
            return redirect(url_for('login'))
    else:
        flash('Usuario sem acesso!', 'alert-danger')
        return redirect(url_for('login'))


@app.route('/autenticaldap')
def autenticarPorLdap():
    ip_client = request.remote_addr
    batcmd = "wmic / node: {} computersystem get username".format(ip_client)
    usuario_pc = str(subprocess.check_output(batcmd, shell=True)).split("\\")[5].strip()

    list_usuario = ['mlarosario', 'cobarrinha', 'lgggrangeiro']
    if usuario_pc in list_usuario:

        con = ldap.initialize("ldap://10.201.0.250:389")
        user_dn = r"mvaut@ascs.intra"
        password = "Mudar@123"
        criteria = ldap.filter.filter_format('(&(objectClass=user)(sAMAccountName=%s))', ['{}'.format(usuario_pc)])

        try:
            con.simple_bind_s(user_dn, password)
            res = con.search_s("OU=ASSOCIACAO,DC=ascs,DC=intra", ldap.SCOPE_SUBTREE, criteria)
            if len(res) > 0:
                session['usuario_logado'] = usuario_pc
                flash(usuario_pc + ' Logou com sucesso! :autenticarPorLdap ', 'alert-success')
                proxima_pagina = request.args.get('proxima')
                print(session['usuario_logado'] + ' - /autenticarPorLdap')
                return redirect(proxima_pagina)
            else:
                flash('Usuário não encontrado, faça Login manualmente!', 'alert-danger')
                return redirect(url_for('login', proxima=url_for('index')))
        except Exception as error:
            usuario_logado = error
            print(error)
        return render_template('teste.html', usuario_logado=usuario_logado, usuario=usuario_pc)
    else:
        flash('Usuário sem acesso!', 'alert-danger')
        return redirect(url_for('login', proxima=url_for('index')))


@app.route('/logout')
def logout():
    session['usuario_logado'] = None
    flash('Usuário deslogado!', 'alert-warning')
    print('/logout - session = ', session['usuario_logado'])
    return redirect(url_for('login'))


@app.route('/teste')
def teste():
    return render_template('teste.html')


if __name__ == "__main__":
    # app.run(debug=True, host='0.0.0.0', port=8001, threaded=True)
    from waitress import serve

    serve(app, host='0.0.0.0', port=8001)
