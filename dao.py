from models import Usuario, Censo
import cx_Oracle
import json

SQL_DELETA_CENSO = 'delete from CENSO_HOSPITALAR  where id = {}'

SQL_CENSO_POR_ID = '''SELECT 
                        nr_guia,
                        acomodacao,
                        ds_hospital,
                        beneficiario,
                        plano,
                        classificacao,
                        quadro_clinico,
                        dt_entrada,
                        dt_saida,
                        diagnostico,
                        ultima_modificacao,
                        id
                        
from CENSO_HOSPITALAR where id = {}'''

SQL_CENSO_POR_NR_GUIA = '''SELECT 
                        nr_guia,
                        acomodacao,
                        ds_hospital,
                        beneficiario,
                        plano,
                        classificacao,
                        quadro_clinico,
                        dt_entrada,
                        dt_saida,
                        diagnostico,
                        ultima_modificacao,
                        id

from CENSO_HOSPITALAR where nr_guia = {}'''

SQL_USUARIO_POR_ID = '''SELECT id, nome, senha from USUARIO_GERENCIA_NIPS where id = '{}' '''
SQL_USUARIO_POR_ID_NOVO = '''select u.CD_USUARIO, u.COLABORADOR, u.LOTACAO from dbaps.INFO_USERS u where u.CD_USUARIO = '{}' '''



SQL_ATUALIZA_CENSO = '''
UPDATE CENSO_HOSPITALAR
 SET 
    nr_guia = '{}',
    acomodacao = '{}',
    ds_hospital = '{}',
    beneficiario = '{}',
    plano = '{}',
    classificacao = '{}',
    quadro_clinico = '{}',
    dt_entrada = '{}',
    dt_saida = '{}',
    diagnostico = '{}',
    ultima_modificacao = '{}' 
 where id = '{}'  '''

SQL_BUSCA_CENSOS = ''' SELECT  
                nr_guia,
                acomodacao,
                ds_hospital,
                beneficiario,
                plano,
                classificacao,
                quadro_clinico,
                dt_entrada,
                dt_saida,
                diagnostico,
                ultima_modificacao,
                id
                  from CENSO_HOSPITALAR'''

SQL_CRIA_CENSO = '''
INSERT into CENSO_HOSPITALAR (
                    nr_guia,
                    acomodacao,
                    ds_hospital,
                    beneficiario,
                    plano,
                    classificacao,
                    quadro_clinico,
                    dt_entrada,
                    dt_saida,
                    diagnostico,
                    ultima_modificacao
                    ) values ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')

'''


class CensoDao:
    def __init__(self, usr,pwd, dsn):
        self.__usr = usr
        self.__pwd = pwd
        self.__dsn = dsn

    def salvar(self, censo):

        with cx_Oracle.connect(user=self.__usr, password=self.__pwd, dsn=self.__dsn, encoding="UTF-8") as connection:
            cursor = connection.cursor()

            if censo.id:
                cursor.execute(SQL_ATUALIZA_CENSO.format(
                    censo.nr_guia,
                    censo.acomodacao,
                    censo.ds_hospital,
                    censo.beneficiario,
                    censo.plano,
                    censo.classificacao,
                    censo.quadro_clinico,
                    censo.dt_entrada,
                    censo.dt_saida,
                    censo.diagnostico,
                    censo.ultima_modificacao,
                    censo.id
                )
                )
            else:

                cursor.execute(SQL_CRIA_CENSO.format(
                    censo.nr_guia,
                    censo.acomodacao,
                    censo.ds_hospital,
                    censo.beneficiario,
                    censo.plano,
                    censo.classificacao,
                    censo.quadro_clinico,
                    censo.dt_entrada,
                    censo.dt_saida,
                    censo.diagnostico,
                    censo.ultima_modificacao
                )
                )
                censo.id = cursor.lastrowid
            connection.commit()

        return censo

    def listar(self):

        with cx_Oracle.connect(user=self.__usr, password=self.__pwd, dsn=self.__dsn, encoding="UTF-8") as connection:
            cursor = connection.cursor()
            cursor.execute(SQL_BUSCA_CENSOS)
            censos = traduz_censos(cursor.fetchall())
        return censos

    def busca_por_id(self, id):

        with cx_Oracle.connect(user=self.__usr, password=self.__pwd, dsn=self.__dsn, encoding="UTF-8") as connection:
            cursor = connection.cursor()
            cursor.execute(SQL_CENSO_POR_ID.format(id))
            tupla = cursor.fetchone()
        return Censo(tupla[0],
                     tupla[1],
                     tupla[2],
                     tupla[3],
                     tupla[4],
                     tupla[5],
                     tupla[6],
                     tupla[7],
                     tupla[8],
                     tupla[9],
                     tupla[10],
                     tupla[11],
                     )


    def busca_por_nr_guia(self, nr_guia):
        with cx_Oracle.connect(user=self.__usr, password=self.__pwd, dsn=self.__dsn, encoding="UTF-8") as connection:
            cursor = connection.cursor()
            cursor.execute(SQL_CENSO_POR_NR_GUIA.format(nr_guia))
            censos = traduz_censos(cursor.fetchall())

        return censos

    def deletar(self, id):
        with cx_Oracle.connect(user=self.__usr, password=self.__pwd, dsn=self.__dsn, encoding="UTF-8") as connection:
            cursor = connection.cursor()
            cursor.execute(SQL_DELETA_CENSO.format(id))
            connection.commit()


def traduz_censos(censos):
    def cria_censo_com_tupla(tupla):

        return Censo(tupla[0],
                     tupla[1],
                     tupla[2],
                     tupla[3],
                     tupla[4],
                     tupla[5],
                     tupla[6],
                     tupla[7],
                     tupla[8],
                     tupla[9],
                     tupla[10],
                     id=tupla[11])
    return list(map(cria_censo_com_tupla, censos))


def traduz_usuario(tupla):
    return Usuario(tupla[0], tupla[1], tupla[2])
